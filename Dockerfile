FROM alpine:latest
LABEL app="PetClinic"
EXPOSE 8080
ENV JAVA_HOME=/usr/lib/jvm/java-1.8-openjdk
ENV PATH=$PATH:$JAVA_HOME/bin
RUN apk update && apk add git && apk add --no-cache openjdk8
WORKDIR /root
RUN git clone https://github.com/spring-projects/spring-petclinic.git
WORKDIR /root/spring-petclinic
ENTRYPOINT ["./mvnw","spring-boot:run"]
