## Project PetClinic to Phoebus Application Documentation 
Este desafio está divido em dois projetos no GitLab: 

- [petclinic-infra](https://gitlab.com/mgaabriellima/terraform): Corresponde ao projeto de criação da infraestrutura.
- [petclinic-app](https://gitlab.com/mgaabriellima/petclinic-app): Corresponde ao projeto de deploy da aplicação PetClinic.

### Objetivo 
- Desenvolver o código necessário para fazer o deploy na AWS da aplicação Petclinic com provisionamento da infraestrutura necessária e automação de CI/CD tanto para a infraestrutura quanto para a aplicação.

### Requisitos
- Aplicação conteinerizada.
- CI da aplicação.

### Recursos
- Docker (Docker Hub)
- Gitlab Repo + CI/CD (Infra e App)
- [App PetClinic](https://github.com/spring-projects/spring-petclinic)


## Arquiteura do Projeto 
A arquiterura desse projeto consiste em realizar o deploy da aplicação PetClinic em um container Docker, realizar alguns testes de CI e o push para o DockerHub. 

| Recursos | Detalhes |
| ---  | ---     |
| Container Docker| - Imagem padrão: alpine<br>- Entrypoint: aplicação PetClinic|
| GitLab CI/CD |Stages:<br>- Build<br>- Apply<br>|

### Container Docker
#### Criação da imagem da aplicação PetClinic 

**Dockerfile**
Foi utilizado a imagem base do alpine sem multi-stage para criação da imagem da aplicação PetClinic. A aplicação executa na porta 8080, mas será acessada através da 80 devido bind da intância ec2 para o container. 
Foi instalado as dependências do PetClinic `java-1.8` e foi configudado o EntryPoint do container para o processo de execução da aplicação.
A aplicação encontra-se executando como root devido tempo de build deste desafio.

### GitLab CI/CD 
##### Criação da pipeline e stages de CI
**.gitlab-ci.yml**
Utilizado para criação dos stages de CI da aplicação PetClinic, desta forma foram criados apenas 2 stages (build e apply), com o objetivo de simular um teste de build e deploy da aplicação enviando a imagem atualizada do container para o DockerHub. 
O stage **apply** foi configurado para executar manualmente por meio de uma issue ou merge request. 

### Considerações
O desafio proposto me rendeu bastante aprendizado e foi bem desafiador concluí-lo. Foquei em entregar o laboratório completo com os recursos essenciais definidos como requisito. Tive dificuldades em definir o escopo do CI desta aplicação devido falta de familiaridade com o CI de uma aplicação real em produção, desta forma desenvolvi um cenário simples e funcional. 
Encontrei problemas com o **docker-on-docker** ao utilizar o docker:dind no GitLab. Ao fazer o build do container Alpine que funcionava localmente não era possível construir a imagem da aplicação dentro da pipeline. Então, utilizei o recurso **--network host** e conseguir fazer o bypass da rede do docker:dind.
https://github.com/gliderlabs/docker-alpine/issues/307
